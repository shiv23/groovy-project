
================================================ Important Links =====================================================

curl & json response related link og groovy jenkins 

https://e.printstacktrace.blog/how-to-catch-curl-response-in-jenkins-pipeline/

========================================================================================================================

pipeline {
    agent any

    stages {
        stage('Env Variables') {
            steps {
                sh "printenv"
                echo "The build number is ${env.BUILD_NUMBER}"
            }
        }
    }
}


====================================================================================================



pipeline {
    agent any
    parameters {
        string(name: 'NAME', description: 'Please tell me your name')
        choice(name: 'GENDER', choices: ['Male', 'Female'], description: 'Choose Gender')
        string(name: 'CITY')
    }
    stages {
        stage('Printing name') {
            steps {
                script {
                    def name = "${params.NAME}"
                    def gender = "${params.GENDER}"
                    def city = "${params.CITY}"
                    if(gender == "Male") {
                        echo "Mr. $name $city"    
                    } else {
                        echo "Mrs. $name"
                    }
                }
            }
        }
   }
}


=============================================================== Print varible using echo  =========================================================================



pipeline {
    agent any
	
    parameters {
        choice choices: ['Australia', 'India', 'Kivi'], description: 'Please Select The Country', name: 'Country'
        string defaultValue: '', description: 'Please Enter Your Name', name: 'Name', trim: false
    }
    stages {
        stage('Printing name') {
            steps {
                script {
                    def name = "${params.Name}"
                    echo "${name}"
                    def country = "${params.Country}"
                    echo "${country}"
                    if(country == "Kivi") {
                        echo "Mr. $name is living in $country"    
                    } else {
                        echo "Mrs. $name is living in other city"
                    }
                }
            }
        }
   }
}

===================================================== Functions In Groovy ===========================================================================

pipeline {
    agent any
    stages {
        stage('Test') {
            steps {
                whateverFunction()
            }
        }
    }
}

void whateverFunction() {
    sh 'ls /'
}

======================================================== Multiple Function In Groovy ==============================================================

pipeline {
    agent any
    stages {
        stage('Check User & Current Working directory') {
            steps {
                whateverFunction()
            }
        }
        stage('listing Of Directory'){
            steps{
                getlist()
            }
        }
    }
}

void whateverFunction() {
    sh 'whoami'
    sh 'pwd'
}

void getlist(){
    sh 'ls -l '
}

========================================================Parametarized With Function Calling ===========================================================

pipeline {
    agent any
	
    parameters {
        choice choices: ['Australia', 'India', 'Kivi'], description: 'Please Select The Country', name: 'Country'
        string defaultValue: '', description: 'Please Enter Your Name', name: 'Name', trim: false
    }
    stages {
        stage('Printing name') {
            steps {
                script {
                    def name = "${params.Name}"
                    echo "${name}"
                    def country = "${params.Country}"
                    echo "${country}"
                    if(country == "Kivi") {
                        abc()
                    } else {
                        echo "Mrs. $name is living in other city"
                    }
                }
            }
        }
   }
}

void abc(){
    echo "Mr. $name is living in $country"
}



================================================================== curl in groovy ==============================================================


pipeline {
    agent any

    stages {
        stage("Using curl example") {
            steps {
                script {
                    final String url = "http://localhost:8080/job/Demos/job/maven-pipeline-demo/job/sdkman/2/api/json"

                    final String response = sh(script: "curl -s $url", returnStdout: true).trim()

                    echo response
                }
            }
        }
    }
}

============================== Active choices parameters [Select instance Running Code with if-else conditon]===============================================================

Install Plugin : Active Choice Plugin

Active choices parameters : 

return['1':'IQ-1','2':'IQ-2','3':'IQ-3']

========================================================  ==========================================


import jenkins.model.*
import groovy.json.JsonOutput


def String ab = "1"
echo "Data in ab variable : $ab"
def String bc = "2"
echo "Data in bc variable : $bc"
def String cd = "3"
echo "Data in cd variable : $cd"


pipeline {
    agent any

    stages {
        stage("I am In instance 1") {
            steps { 
                script {
                    echo 'I am in instance 1 step'
                    def data = "${params.server}"
                    echo "Selected Server Is : ${data}"
                    if(data == ab){
                        echo 'yes'
                    }else{
                        echo 'no'
                    }
                }
            }
        }
        stage("I am In instance 2") {
            steps {
                script {
                    echo 'I am in instance 2 step'
                    def data = "${params.server}"
                    echo "Selected Server Is : ${data}"
                    if(data == bc){
                        echo 'yes'
                    }else{
                        echo 'no'
                    }
                }
            }
        }
        
        stage("I am In instance 3") {
            steps {
                script {
                    echo 'I am in instance 3 step'
                    def data = "${params.server}"
                    echo "Selected Server Is : ${data}"
                    if(data == cd){
                        echo 'yes'
                    }else{
                        echo 'no'
                    }
                }
            }
        }
       
}
}

============================================================ function define & call function in groovy ==============================================

import jenkins.model.*
import groovy.json.JsonOutput


def String ab = "1"
echo "Data in ab variable : $ab"
def String bc = "2"
echo "Data in bc variable : $bc"
def String cd = "3"
echo "Data in cd variable : $cd"


pipeline {
    agent any

    stages {
        stage("I am In instance 1") {
            steps { 
                script {
                    echo 'Function define & fetch parameter'
                    def data = "${params.server}"
                    echo "Selected Server Is : ${data}"
                    if(data == ab){
                        echo 'instance 1'
                        instance_1_access()
                    }else if (data == bc){
                        echo 'instance 2'
                        instance_2_access()
                    }else if (data == cd){
                        instance_3_access()
                    }
                } 
            }
        }
            
    }
}

void instance_1_access(){
    echo 'I am in function instance 1'
}

void instance_2_access(){
    echo 'I am in function instance 2'
}

void instance_3_access(){
    echo 'I am in function instance 3'
}

=====================================================================================================================================================